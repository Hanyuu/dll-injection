#include <iostream>
#include <string>
#include <windows.h>
#include <TlHelp32.h.>
using namespace std;



DWORD FindProcessId(const char *processname)
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	DWORD result = NULL;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hProcessSnap) return(FALSE);

	pe32.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(hProcessSnap, &pe32))
	{
		CloseHandle(hProcessSnap);          

		return(NULL);
	}

	do
	{

		char output[256];
		sprintf_s(output, "%ws", pe32.szExeFile);
		if (0 == strcmp(processname, output))
		{
			result = pe32.th32ProcessID;
			break;
		}
	} while (Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);

	return result;
}

bool inject(DWORD pId, char *dllName)
{
	HANDLE h = OpenProcess(PROCESS_ALL_ACCESS, false, pId);
	LPVOID LoadLibAddr = (LPVOID)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");

	LPVOID dereercomp = VirtualAllocEx(h, NULL, strlen(dllName), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	bool written = WriteProcessMemory(h, dereercomp, dllName, strlen(dllName), NULL);
	HANDLE asdc = CreateRemoteThread(h, NULL, NULL, (LPTHREAD_START_ROUTINE)LoadLibAddr, dereercomp, 0, NULL);
	WaitForSingleObject(asdc, INFINITE);
	VirtualFreeEx(h, dereercomp, strlen(dllName), MEM_RELEASE);
	CloseHandle(asdc);
	CloseHandle(h);
	return true;
}

int main()
{
	char * process_name = "firefox.exe";
	
	
	DWORD pid = FindProcessId(process_name);
	printf("\nPID %i\n",pid);
	inject(pid, "inject_dll.dll.dll");
	printf("\nError in injection is %d ", GetLastError());
	//
	system("pause");
	return 0;
}